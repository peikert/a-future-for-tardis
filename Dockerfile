FROM rocker/r-ver:4.0.4
ARG BUILD_DATE=2021-06-01
WORKDIR /home/rstudio
RUN install2.r --error --skipinstalled \ 
  furrr \ 
  future \
  future.batchtools \ 
  future.apply \
  remotes \
  sessioninfo
RUN installGithub.r \ 
  brandmaier/semtree@189b181
